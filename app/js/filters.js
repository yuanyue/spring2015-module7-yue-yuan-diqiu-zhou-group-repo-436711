'use strict';

/* Filters */

angular.module('phonecatFilters', []).filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
}).filter('checkifavailible', function(){
	return function(input) {
		// how to maintain normal input
		return input ? input : "Not Availible";
	};
});
